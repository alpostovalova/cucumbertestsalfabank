# new feature
# Tags: optional
    

Feature: Check product price

    Scenario: I check Beats earphones which have price from 17000 rubles to 25000 rubles
      Given I open yandex site
      And I open yandex market
      And I open electronics section
      When I find Beats earphones
      And I set price from "17000" rubles to "25000" rubles
      And I save name of the first earphones in the list
      And I open description of the first earphones
      Then I find the name and names should be coincide


    Scenario: I check phone which has price from 40000 rubles
      Given I open yandex site
      And I open yandex market
      And I open electronics section
      When I find Samsung phone
      And I set "40,000" as the minimum price
      And I save name of the first phone in the list
      And I open description of the first phone
      Then I find the name and names should be coincide
