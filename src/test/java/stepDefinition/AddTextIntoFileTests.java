package stepDefinition;

import Helper.FileWorker;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import org.openqa.selenium.support.PageFactory;
import pages.AlfaPages.AlfaAboutPage;
import pages.AlfaPages.AlfaJobsPage;
import pages.AlfaPages.AlfaMainPage;
import pages.AlfaPages.SearchPage;

public class AddTextIntoFileTests {

    SearchPage searchPage = PageFactory.initElements(Hooks.driver, SearchPage.class);
    AlfaMainPage alfaMainPage = PageFactory.initElements(Hooks.driver, AlfaMainPage.class);
    AlfaJobsPage alfaJobsPage = PageFactory.initElements(Hooks.driver, AlfaJobsPage.class);
    AlfaAboutPage alfaAboutPage = PageFactory.initElements(Hooks.driver, AlfaAboutPage.class);

    @Given("^I open \"(.*)\" page$")
    public void IOpenPageForSearching(String searcher){
        //так и не придумала как получить любой поисковик
        searchPage.NavigateToGoogle(searcher);
        Hooks.searcher = searcher;
    }

    @Given("^I enter \"(.*)\" to URL$")
    public void IEnterSearchRequestToURL(String request){
        searchPage.openResultOfSearch(request);
    }

    @Given("^I find the site of AlfaBank and open it$")
    public void IFindSiteAndOpenIt(){
        searchPage.findSiteAndOpen();
    }

    @Given("^I open jobs page$")
    public void IOpenAlfaJobsPage(){
        alfaMainPage.openAlfaJobsPage();
    }

    @And("^I open About section$")
    public void IOpenAboutAlfaSection(){
        alfaJobsPage.clickAlfaAboutLink();
    }

    @And("^I find and write this text to file$")
    public void IFindAndCopyText(){
        alfaAboutPage.findAndCopyText();
    }
}
