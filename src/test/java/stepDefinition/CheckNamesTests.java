package stepDefinition;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.support.PageFactory;
import pages.YandexPages.*;


public class CheckNamesTests {

    YandexPage yandexPage = PageFactory.initElements(Hooks.driver, YandexPage.class);
    MarketPage marketPage = PageFactory.initElements(Hooks.driver,MarketPage.class);
    PhonesPage phonesPage = PageFactory.initElements(Hooks.driver,PhonesPage.class);
    DescriptionPage descriptionPage = PageFactory.initElements(Hooks.driver, DescriptionPage.class);
    EarphonesPage earphonesPage = PageFactory.initElements(Hooks.driver, EarphonesPage.class);

    String valueOfTitle;


    @Given("^I open yandex site$")
    public void INavigateToYandexSite ()
    {
        yandexPage.NavigateToBasePage();
    }

    @Given("^I open yandex market$")
    public void INavigateToYandexMarket() throws Throwable{
        yandexPage.IClickMarketLink();
    }

    @Given("^I open electronics section$")
    public void INavigateToElectronicsSection(){
        marketPage.IClickElectronicsLink();
    }

    @When("^I find Samsung phone$")
    public void IFilterThePageByTheCompanies(){
        marketPage.IClickPhonesLink();
        phonesPage.findCompanyAndClick("Samsung");
    }

    @When("^I find Beats earphones$")
    public void IFilterTheBeatsEarphones(){
        marketPage.IClickEarphonesLink();
        earphonesPage.findCompanyAndClick("Beats");
    }

    @When("^I set price from \"(.*)\" rubles to \"(.*)\" rubles$")
    public void ISetPriceForEarphones(int from, int to){
        earphonesPage.setPriceFrom(from);
        earphonesPage.setPriceTo(to);
    }

    @When("^I set \"(.*)\" as the minimum price$")
    public void ISetMinimumPrice(int from){
        phonesPage.setPriceFrom(from);
    }

    @When("^I save name of the first phone in the list$")
    public void IFindTheFirstPhoneAndSaveItName(){
        valueOfTitle = phonesPage.findTheFirstPhoneInTheTable();
    }

    @When("^I save name of the first earphones in the list$")
    public void IFindTheFirstEarphoneAndSaveItName(){
        valueOfTitle = earphonesPage.findTheFirstEarphonesInTheTable();
    }

    @When("^I open description of the first phone$")
    public void IOpenDescriptionForTheFirstPhone() throws InterruptedException {
        phonesPage.openTheFirstPhoneDescription(valueOfTitle);
    }

    @When("^I open description of the first earphones$")
    public void IOpenDescriptionForTheFirstEarpnones(){
        earphonesPage.openTheFirstEarphonesDescription(valueOfTitle);
    }

    @Then("^I find the name and names should be coincide$")
    public void IFindPhoneNameOnDescriptionPage(){
        descriptionPage.savePhoneName(valueOfTitle);

    }

/*
    @Then("^Names should be coincide$")
    public void ICheckPhoneNames(){
        Assert.assertEquals(phoneName,phoneNameInDescritiption);
    }
*/
}
