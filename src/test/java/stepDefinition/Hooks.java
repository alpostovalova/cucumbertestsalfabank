package stepDefinition;

import Helper.FileWorker;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.util.Calendar;
import java.util.Date;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.*;


public class Hooks {
    static WebDriver driver;
    static Date startDate;
    static Capabilities caps;
    static String searcher;

    @Before
    public static void BeforeTest(){
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        //получаем параметры для формирования названия файла
        startDate = Calendar.getInstance().getTime();
        caps = ((RemoteWebDriver)driver).getCapabilities();
    }


    @After
    public void AfterTest(){
        driver.quit();
    }

    public static Date getStartDate(){
        return startDate;
    }

    public static String getBrowserName(){
        String browserName = caps.getBrowserName();
        String browserVersion = caps.getVersion();
        String browser = (browserName + " " + browserVersion).toUpperCase();
        return  browser;
    }

    public static String getSearcher(){
        return searcher.replaceAll("https://", "");
    }

}


