package pages.YandexPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class EarphonesPage {
    WebDriver driver;

    public EarphonesPage(WebDriver driver){
        this.driver = driver;
    }

    By listPopularCompanies = By.xpath("//ul[@data-reactid='56']/li");
    By priceFrom = By.id("glpricefrom");
    By priceTo = By.id("glpriceto");
    By titleTheFirstEarphones = By.cssSelector(".n-snippet-cell2__title");


    public void findCompanyAndClick(String company) {
        //получаем список строк в таблице
        List<WebElement> options = driver.findElements(listPopularCompanies);
        for (WebElement option : options) {
            if (company.equals(option.getText())) {
                option.click();
                System.out.println("Company " + company + " is selected");
            }

        }
    }

    public void setPriceFrom(int price_from) {
        driver.findElement(priceFrom).sendKeys(String.valueOf(price_from));
        System.out.println("Price from has value " + price_from);
    }

    public void setPriceTo(int price_to) {
        driver.findElement(priceTo).sendKeys(String.valueOf(price_to));
        System.out.println("Price to has value " + price_to);
    }

    public String findTheFirstEarphonesInTheTable(){
        String earphonesName = "";
        try {
            //ждем пока прогрузиться обновленный список товаров
            Thread.sleep(3000);
            earphonesName = driver.findElement(titleTheFirstEarphones).getText();
            System.out.println("The first earphones in the table has " + earphonesName + " name");
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
        return earphonesName;
    }

    public void openTheFirstEarphonesDescription(String textLink) {
        driver.findElement(By.linkText(textLink)).click();
        System.out.println("The first phone description is opened");
    }
}
