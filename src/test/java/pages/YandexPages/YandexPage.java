package pages.YandexPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class YandexPage {
    WebDriver driver;

    public static String baseURL = "https://www.yandex.ru/";

    public YandexPage(WebDriver driver){
        this.driver = driver;
    }

    By marketLink = By.cssSelector("[data-id='market']");


    public void IClickMarketLink(){
        driver.findElement(marketLink).click();
        System.out.println("Yandex market is opened");
    }

    public void NavigateToBasePage() {
        driver.get(baseURL);
        System.out.println("Navigate to Yandex");
    }
}
