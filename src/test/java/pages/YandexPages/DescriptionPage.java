package pages.YandexPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class DescriptionPage {
    WebDriver driver;

    public DescriptionPage(WebDriver driver){
        this.driver = driver;
    }

    By productNameInTitle = By.cssSelector(".n-title__text");

    public void savePhoneName( String productName){
        //получаем список из названия и лейбла "рекомендация"
        List<WebElement> options = driver.findElements(productNameInTitle);
        for (WebElement option : options) {
            if (productName.equals(option.getText())) {
                System.out.println("This product has " + productName + " name");
                return;
            }
        }
    }
}
