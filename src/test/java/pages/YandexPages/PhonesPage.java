package pages.YandexPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PhonesPage {
    WebDriver driver;

    public PhonesPage(WebDriver driver){
        this.driver = driver;
    }

    By showAllButtonForCompanies = By.cssSelector("[data-reactid='152']");
    By listPopularCompanies = By.xpath("//*[@id=\"search-prepack\"]/div/div/div[2]/div/div[1]/div[3]/fieldset/ul/li[10]/div");
    By samsung = By.name("Производитель Samsung");
    By priceFrom = By.id("glpricefrom");
    By priceTo = By.id("glpriceto");
    By TitletheFirstPhone = By.cssSelector(".n-snippet-cell2__title");



    public void clickShowAllButtonForCompanies(){
        driver.findElement(showAllButtonForCompanies).click();
        System.out.println("I see all phone companies");
    }

    public void findCompanyAndClick(String phoneCompany) {
       /* List<WebElement> options = driver.findElements(listPopularCompanies);
        for (WebElement option : options) {
            if (phoneCompany.equals(option.getText())) {
                option.click();
                System.out.println("Company " + phoneCompany + " is selected");
            }

        } */
        //перепробовала все что смогла, не нажимает на производителя
        try {
            Thread.sleep(3000);
            driver.findElement(samsung).click();
            System.out.println("Company is selected");
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
    }

    public void setPriceFrom(int price){
        driver.findElement(priceFrom).sendKeys(String.valueOf(price));
        System.out.println("Price from has value " + price);
    }

    public void setPriceTo(int price){
        driver.findElement(priceTo).sendKeys(String.valueOf(price));
        System.out.println("Price from has value " + price);
    }

    public String findTheFirstPhoneInTheTable()
    {
        String phoneName = "";
        try {
            Thread.sleep(3000);  //ToDo заменить на явный вейт
            phoneName = driver.findElement(TitletheFirstPhone).getText();
            System.out.println("The first phone in the table has " + phoneName + " name");
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
        return phoneName;
    }

    public void openTheFirstPhoneDescription(String textLink) throws InterruptedException {
        driver.findElement(By.linkText(textLink)).click();
        System.out.println("The first phone description is opened");
    }


}
