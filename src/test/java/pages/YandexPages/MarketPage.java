package pages.YandexPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MarketPage {
    WebDriver driver;

    public static String baseURL = "https://market.yandex.ru/";

    public MarketPage(WebDriver driver){
        this.driver = driver;
    }

    By electronicsLink = By.cssSelector("[data-department='Электроника']");
    By phonesLink = By.linkText("Мобильные телефоны");
    By earphonesLink = By.linkText("Наушники и Bluetooth-гарнитуры");

    public void navigateToYandexPage() {
        driver.get(baseURL);
    }

    public void IClickElectronicsLink(){
        driver.findElement(electronicsLink).click();
        System.out.println("Electronics section is opened");
    }

    public void IClickPhonesLink(){
        driver.findElement(phonesLink).click();
        System.out.println("Phones page is opened");
    }

    public void IClickEarphonesLink(){
        driver.findElement(earphonesLink).click();
        System.out.println("Earphones page is opened");
    }


}
