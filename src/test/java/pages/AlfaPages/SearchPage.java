package pages.AlfaPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SearchPage {
    WebDriver driver;

    public SearchPage(WebDriver driver){
        this.driver = driver;
    }

    By searchField = By.id("lst-ib");
    By alfaLink = By.linkText("Альфа-Банк – Альфа-Банк");

    public void NavigateToGoogle(String baseURL){
        driver.get(baseURL);
    }

    public void openResultOfSearch(String searchRequest){
        driver.findElement(searchField).sendKeys(searchRequest);
        driver.findElement(searchField).submit();
    }

    public void findSiteAndOpen() {
        driver.findElement(alfaLink).click();
    }
}
