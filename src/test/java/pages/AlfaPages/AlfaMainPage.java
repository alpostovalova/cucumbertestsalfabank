package pages.AlfaPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import stepDefinition.Hooks;

import java.util.Set;

public class AlfaMainPage {
    WebDriver driver;


    public AlfaMainPage(WebDriver driver){
        this.driver = driver;
    }

    By jobsLink = By.linkText("Вакансии");

    public void openAlfaJobsPage()
    {
        SwitchToAnotherTab();
        driver.findElement(jobsLink).click();
    }

    public void SwitchToAnotherTab(){
        //закрываем вкладку с поиском и остаемся на второй
        String handleHost = driver.getWindowHandle();
        try {
            String parentWindow = driver.getWindowHandle();
            Set<String> handles =  driver.getWindowHandles();
            for(String windowHandle  : handles)
            {
                if(!windowHandle.equals(parentWindow))
                {
                    driver.close();
                    driver.switchTo().window(windowHandle);
                }
            }

        } catch (Exception e) {
            System.err.println("Couldn't get to second page");
        }
    }
}
