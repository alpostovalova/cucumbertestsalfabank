package pages.AlfaPages;

import Helper.FileWorker;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class AlfaAboutPage {
    WebDriver driver;
    FileWorker fileWorker = new FileWorker();


    public AlfaAboutPage(WebDriver driver){
        this.driver = driver;
    }

    By message = By.className("message");
    By info = By.className("info");

    public void findAndCopyText(){
        String text = driver.findElement(message).getText()+ "\n" + driver.findElement(info).getText();
        fileWorker.writeToFile(text);

    }
}
