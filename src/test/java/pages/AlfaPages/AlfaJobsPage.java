package pages.AlfaPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import stepDefinition.Hooks;

public class AlfaJobsPage {
    WebDriver driver;

    Hooks page = new Hooks();

    public AlfaJobsPage(WebDriver driver){
        this.driver = driver;
    }

    By helpLink = By.xpath("//A[@href='/about/']");

    public void clickAlfaAboutLink(){
        driver.findElement(helpLink).click();
    }
}
