package Helper;

import stepDefinition.Hooks;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class FileWorker {
    static DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
    static String searcher;

    public FileWorker() {
    }

    public void writeToFile(String text){
        try{
            // Create new file
            File file = new File( "Time. " + formatter.format(Hooks.getStartDate()) + " Browser. " + Hooks.getBrowserName() + " Searcher. " + Hooks.getSearcher() + ".txt");

            // If file doesn't exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);

            // Write in file
            bw.write(text);

            // Close connection
            bw.close();
            fw.close();
        }
        catch(Exception e){
            System.out.println(e);
        }
    }


}
